import { createRouter, createWebHistory } from 'vue-router';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: { name: 'myGroups' }
    },
    {
      path: '/forbidden',
      name: 'forbidden',
      component: () => import('../views/Forbidden.vue')
    },
    {
      path: '/groepen',
      name: 'myGroups',
      component: () => import('../views/MyGroups.vue')
    },
    {
      path: '/groepen/toevoegen',
      name: 'addGroup',
      component: () => import('../views/group/AddGroup.vue')
    },
    {
      path: '/groepen/:groupId/:groupName',
      name: 'group',
      component: () => import('../views/group/Group.vue'),
      props: true
    },
    {
      path: '/groepen/:groupId/:groupName/bewerken',
      name: 'editGroup',
      component: () => import('../views/group/EditGroup.vue'),
      props: true
    },
    {
      path: '/groepen/:groupId/:groupName/evenementen/toevoegen',
      name: 'addEvent',
      component: () => import('../views/event/AddEvent.vue'),
      props: true
    },
    {
      path: '/groepen/:groupId/:groupName/evenementen/:eventId/:eventName',
      name: 'event',
      component: () => import('../views/event/Event.vue'),
      props: true
    },
    {
      path: '/groepen/:groupId/:groupName/evenementen/:eventId/:eventName/bewerken',
      name: 'editEvent',
      component: () => import('../views/event/EditEvent.vue'),
      props: true
    },
    {
      path: '/groepen/:groupId/:groupName/evenementen/:eventId/:eventName/verlanglijstjes/toevoegen',
      name: 'addWishlist',
      component: () => import('../views/wishlist/AddWishlist.vue'),
      props: true
    },
    {
      path: '/groepen/:groupId/:groupName/evenementen/:eventId/:eventName/verlanglijstjes/:wishlistId/bewerken',
      name: 'editWishlist',
      component: () => import('../views/wishlist/EditWishlist.vue'),
      props: true
    }
  ]
});

export default router;
