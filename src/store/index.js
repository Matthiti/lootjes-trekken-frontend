import { defineStore } from 'pinia';
import { ref } from 'vue';

import config from '../config';
import { axios } from '../services/http';

export const useStore = defineStore('store', () => {
  const tokenReady = ref(false);
  const loadingCounter = ref(0);
  const user = ref(null);
  const users = ref([]);

  const setTokenReady = () => {
    tokenReady.value = true;
  };

  const incrementLoadingCounter = () => {
    loadingCounter.value++;
  };

  const decrementLoadingCounter = () => {
    if (loadingCounter.value > 0) loadingCounter.value--;
  };

  const fetchUser = () => {
    return new Promise((resolve, reject) => {
      axios
        .get(`${config.ssoBackendUrl}/api/v1/me`)
        .then((res) => {
          user.value = res.data;
          resolve(user.value);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  const fetchUsers = () => {
    return new Promise((resolve, reject) => {
      axios
        .get(`${config.ssoBackendUrl}/api/v1/users`)
        .then((res) => {
          users.value = res.data;
          resolve(users.value);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  return {
    tokenReady,
    loadingCounter,
    user,
    users,
    setTokenReady,
    incrementLoadingCounter,
    decrementLoadingCounter,
    fetchUser,
    fetchUsers
  };
});
