import axios from 'axios';

import config from '../config';
import router from '../router';
import { useStore } from '../store';

axios.defaults.baseURL = config.apiUrl;

window.addEventListener('tokenLoaded', (e) => {
  const store = useStore();

  const token = e.detail;
  axios.defaults.headers.common['X-Authorization'] = `Bearer ${token}`;

  axios.interceptors.request.use((config) => {
    store.incrementLoadingCounter();
    return config;
  });

  axios.interceptors.response.use(
    (res) => {
      store.decrementLoadingCounter();
      return res;
    },
    (err) => {
      store.decrementLoadingCounter();
      if (err.response) {
        if (err.response.status === 401) {
          window.location.href = `${config.ssoUrl}?redirect=${window.location.href}`;
        } else if (err.response.status === 403 && router.currentRoute.name !== 'forbidden') {
          router.push({ name: 'forbidden' });
        }
      }

      return Promise.reject(err);
    }
  );

  store.setTokenReady();
  store.fetchUser();
  store.fetchUsers();
});

export { axios };
