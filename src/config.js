const apiUrl = '/api/v1';
// const ssoUrl = 'http://localhost:8080';
// const ssoBackendUrl = 'http://localhost:8000';
const ssoUrl = 'https://login.roelink.eu';
const ssoBackendUrl = 'https://sso.roelink.eu';

export default {
  apiUrl,
  ssoUrl,
  ssoBackendUrl
};
