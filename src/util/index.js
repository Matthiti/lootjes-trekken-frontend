const nameToUrl = (name) => {
  return encodeURIComponent(name.toLowerCase().replaceAll(' ', '-'));
};

export { nameToUrl };
